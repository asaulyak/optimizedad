import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {CurrencyPipe, DatePipe, PercentPipe} from '@angular/common';

interface FoodNode {
  name: string;
  children?: FoodNode[];
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.less']
})
export class TableComponent implements OnInit {

  private formats = {
    currency: this.currencyPipe,
    percent: this.percentPipe,
    date: this.datePipe,
  };

  @Input()
  dataSource$ = new BehaviorSubject<{ [name: string]: any }>([]);

  @Input()
  displayedColumns$ = new BehaviorSubject<string[]>([]);

  @Output()
  rowClick = new EventEmitter();

  tableDataSource$ = new BehaviorSubject<any[]>([]);

  columnNames: string[];

  constructor(private currencyPipe: CurrencyPipe, private percentPipe: PercentPipe, private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.dataSource$.subscribe(changedHeroData => {
      this.tableDataSource$.next(this.flatten(changedHeroData));
    });

    this.displayedColumns$.subscribe(columns => {
      this.columnNames = columns.map(column => column['name']);
    });
  }

  format(input: string, format: string): string {
    const pipe = this.formats[format];

    return pipe ? pipe.transform(input) : input;
  }

  level(row, i: number) {
    return {[`l${row.level || 0}`]: !i};
  }

  onRowClick(row) {
    this.rowClick.next(row);
  }

  private flatten(data, level = 0): any[] {
    return Object.values(data)
      .reduce((acc: any[], current: any) => {
        acc.push(Object.assign({}, current, {level}));

        if (current.children) {
          Array.prototype.push.apply(acc, this.flatten(current.children, level + 1));
        }

        return acc;
      }, []) as any[];
  }
}
