import {Observable} from 'rxjs';

export interface Config {
  // TODO: Add provider typedef
  providers: any;
  columns: Column[];
  toolbar?: any;
}

export interface Column {
  name: string;
  display: string;
  format?: string;
}
