import {Component, Input, OnInit} from '@angular/core';
import {Config} from './config';
import {BehaviorSubject, forkJoin, of} from 'rxjs';
import {map} from 'rxjs/operators';
import mergeDeepRight from 'ramda/src/mergeDeepRight';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.less']
})
export class ReportComponent implements OnInit {
  @Input()
  config: Config;

  // TODO: Move to config
  defaultProvider = 'campaigns';
  tableDataSource = new BehaviorSubject<any>({});

  private selectedGroups = {};

  constructor() {
  }

  ngOnInit() {
    this.updateTableData();
  }

  updateTableData() {
    // Fetch table data from all the resources and merge the results
    forkJoin((this.config.providers[this.defaultProvider] || []).map(fetch => fetch()))
      .pipe(map(data => data.reduce((acc, current) => mergeDeepRight(acc, current), {})))
      .subscribe(data => this.tableDataSource.next(data));
  }

  columnsData() {
    return of(this.config.columns);
  }

  onRowClick(row) {
    const source = this.selectedGroups[row.level + 1];

    if (source) {
      this.loadSubLevel(row, source);
    }
  }

  onGroupSelected(event) {
    Object.assign(this.selectedGroups, event);
  }

  private loadSubLevel(row, provider: string, filters?: any) {
    const currentDataSource = {...this.tableDataSource.value};
    const selectedRow = this.findRow(row.id, currentDataSource);
    if (selectedRow) {
      forkJoin((this.config.providers[provider] || []).map(fetch => fetch(filters)))
        .pipe(map(data => data.reduce((acc, current) => mergeDeepRight(acc, current), {})))
        .subscribe(data => {
          selectedRow.children = data;

          this.tableDataSource.next(currentDataSource);
        });
    }
  }

  private findRow(id: string, tree: any) {
    const values = Object.values(tree);

    for (let i = 0; i < values.length; i++) {
      const value = values[i] as any;

      if (value.id === id) {
        return value;
      } else if (value.children) {
        const subRow = this.findRow(id, value.children);

        if (subRow) {
          return subRow;
        }
      }
    }
  }
}
