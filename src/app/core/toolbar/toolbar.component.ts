import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

/**
 * NOTE: Toolbar is hardcoded to display only desired 3 groups
 */
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent implements OnInit {

  @Input()
  config: any;
  sources = [];

  @Output()
  groupSelected = new EventEmitter();

  private selectedSources = {};

  constructor() {
  }

  ngOnInit() {
    this.sources = this.config.sources;
  }

  availableSources(group) {
    const selectedGroups = Object.keys(this.selectedSources).filter(key => key !== `${group}`);

    return this.sources
      .filter(source => !selectedGroups.some(key => this.selectedSources[key] === source));
  }

  selectSource(event, group) {
    this.selectedSources[group] = event.value;

    this.groupSelected.next({
      [group]: event.value
    });
  }
}
