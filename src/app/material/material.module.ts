import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule, MatIconModule, MatRadioModule, MatSelectModule, MatTableModule, MatTreeModule} from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule,
    MatTableModule,
    MatTreeModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule
  ],
  exports: [
    MatButtonModule,
    MatTableModule,
    MatTreeModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule
  ]
})
export class MaterialModule { }
