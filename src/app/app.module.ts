import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TableComponent} from './core/table/table.component';
import {CdkTableModule} from '@angular/cdk/table';
import {MaterialModule} from './material/material.module';
import {CurrencyPipe, DatePipe, PercentPipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusinessComponent} from './pages/business/business.component';
import {ReportComponent} from './core/report/report.component';
import {ToolbarComponent} from './core/toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ReportComponent,
    BusinessComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CdkTableModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    PercentPipe,
    CurrencyPipe,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
