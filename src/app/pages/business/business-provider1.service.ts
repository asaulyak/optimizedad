import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class BusinessProvider1Service {

  constructor() {
  }

  getCampaigns() {
    return of(
      {
        'facebook': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'facebook1': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google1': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'facebook2': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google2': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'facebook3': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google3': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'facebook4': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google4': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'facebook5': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
        'google5': {
          id: uuid.v4(),
          'cost': 1254588,
          'p/l': 1.1,
          'roi': .95,
          'cpl': 16,
          'rpl': 22
        },
      }
    );
  }

  getBuyers() {
    return of(
      {
        'buyer1': {
          '%ofLeads': 35.3,
          cost: 1254588,
          cpl: 16,
          id: uuid.v4(),
          leads: 12,
          'p/l': 1.1,
          revenueCalls: 1.53,
          revenueFull: 115,
          revenueLeads: 10,
          roi: 0.95,
          rpl: 22,
          source: 'Buyer 1'
        }
      }
    );
  }

  getDaily() {
    return of(
      {
        '2:00am': {
          '%ofLeads': 35.3,
          cost: 1254588,
          cpl: 16,
          id: uuid.v4(),
          leads: 12,
          'p/l': 1.1,
          revenueCalls: 1.53,
          revenueFull: 115,
          revenueLeads: 10,
          roi: 0.95,
          rpl: 22,
          source: '2:00 am'
        }
      }
    );
  }
}
