import { TestBed } from '@angular/core/testing';

import { BusinessProvider1Service } from './business-provider1.service';

describe('BusinessProvider1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessProvider1Service = TestBed.get(BusinessProvider1Service);
    expect(service).toBeTruthy();
  });
});
