import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class BusinessProvider2Service {

  constructor() {
  }

  getCampaigns() {
    return of({
      'facebook': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115

      },
      'google': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'facebook1': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'google1': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'facebook2': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'google2': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'facebook3': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'google3': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'facebook4': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'google4': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'facebook5': {
        id: uuid.v4(),
        'source': 'Facebook',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
      'google5': {
        id: uuid.v4(),
        'source': 'Google',
        'leads': 12,
        '%ofLeads': 35.3,
        'revenueLeads': 10,
        'revenueCalls': 1.53,
        'revenueFull': 115
      },
    });
  }
}
