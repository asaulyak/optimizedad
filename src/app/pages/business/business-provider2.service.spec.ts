import { TestBed } from '@angular/core/testing';

import { BusinessProvider2Service } from './business-provider2.service';

describe('BusinessProvider2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessProvider2Service = TestBed.get(BusinessProvider2Service);
    expect(service).toBeTruthy();
  });
});
