import {Component, OnInit} from '@angular/core';
import {Config} from '../../core/report/config';
import {BusinessProvider1Service} from './business-provider1.service';
import {BusinessProvider2Service} from './business-provider2.service';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.less']
})
export class BusinessComponent implements OnInit {
  reportConfig: Config = {
    providers: [],
    columns: []
  };

  constructor(private businessService1: BusinessProvider1Service, private businessService2: BusinessProvider2Service) {

  }

  ngOnInit() {
    // Component needs to provide a config for report generation
    this.reportConfig = {
      columns: [
        {
          name: 'source',
          display: 'Source'
        },
        {
          name: 'leads',
          display: 'Leads'
        },
        {
          name: '%ofLeads',
          display: '% of Leads',
          format: 'percent'
        },
        {
          name: 'revenueLeads',
          display: 'Revenue Leads',
          format: 'currency'
        },
        {
          name: 'revenueCalls',
          display: 'Revenue Calls',
          format: 'currency'
        },
        {
          name: 'revenueFull',
          display: 'Revenue Full',
          format: 'currency'
        },
        {
          name: 'cost',
          display: 'Cost',
          format: 'currency'
        },
        {
          name: 'p/l',
          display: 'P/L',
          format: 'currency'
        },
        {
          name: 'roi',
          display: 'ROI',
          format: 'percent'
        },
        {
          name: 'cpl',
          display: 'CPL',
          format: 'currency'
        },
        {
          name: 'rpl',
          display: 'RPL',
          format: 'currency'
        }
      ],
      providers: {
        campaigns: [
          this.businessService1.getCampaigns,
          this.businessService2.getCampaigns
        ],
        buyers: [
          this.businessService1.getBuyers
        ],
        hourly: [
          this.businessService1.getDaily
        ],
        daily: [
          this.businessService1.getDaily
        ]
      },
      toolbar: {
        sources: [
          'campaigns',
          'buyers',
          'hourly',
          'daily'
        ]
      }
    };
  }
}
